# Logistics - microservices

Ce dépôt contient les briques [SHOPPING] déjà implémentées d'un exercice de microservices simulant une activité logistique.

## Setup

L'app ici présente, qui concentre différents systèmes, utilise (Heroku)[https://www.thisdot.co/blog/deploying-nx-workspace-based-angular-and-nestjs-apps-to-heroku] pour son déploiement.

Il est nécessaire de plus de créer une Config Var "MONGO_URL" en production pour indiquer l'url de la base de données.