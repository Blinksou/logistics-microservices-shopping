<?php

namespace App\Client;

use App\Entity\DTO\Output\PurchasedProductDto;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class OrderClient
{
    public const BASE_URL = "https://blobfish-tech.herokuapp.com/api";

    public function __construct(
        private HttpClientInterface $client,
        private SerializerInterface $serializer
    )
    {
    }

    public function order(array|PurchasedProductDto $purchasedProductDto): array
    {
        $response = $this->client->request(
            Request::METHOD_POST,
            self::BASE_URL . "/order",
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode($purchasedProductDto, JSON_THROW_ON_ERROR)
            ]
        );

        return $response->toArray();
    }
}