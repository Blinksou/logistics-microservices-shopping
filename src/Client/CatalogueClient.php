<?php

namespace App\Client;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class CatalogueClient
{
    public const BASE_URL = "https://fhemery-logistics.herokuapp.com/api";

    public function __construct(
        private HttpClientInterface $client,
    )
    {
    }

    public function getProduct(string $productId): array
    {
        $response = $this->client->request(
            'GET',
            self::BASE_URL . "/products/$productId"
        );

        return $response->toArray();
    }

}