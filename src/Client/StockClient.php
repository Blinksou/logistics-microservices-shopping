<?php

namespace App\Client;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class StockClient
{
    public function __construct(
        private HttpClientInterface $client
    )
    {
    }

    private const BASE_URL = "https://logistics-microservices-stock.herokuapp.com/api/";

    public function getProducts(): array
    {
        $response = $this->client->request(
            'GET',
            self::BASE_URL . "/stock"
        );

        return $response->toArray();
    }
}
