<?php

namespace App\Service;

use App\Client\OrderClient;
use App\Entity\Basket;
use App\Entity\DTO\Output\PurchasedProductDto;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class OrderService
{
    public function __construct(
        private OrderClient         $orderClient,
        private NormalizerInterface $normalizer
    )
    {
    }

    public function order(Basket $basket): array
    {
        $purchasedProductList = [];
        foreach ($basket->getProducts() as $productId => $quantity) {

            $purchasedProduct = new PurchasedProductDto();

            $purchasedProduct->id = $productId;
            $purchasedProduct->quantity = $quantity;

            $purchasedProductList[] = $this->normalizer->normalize($purchasedProduct, 'json');
        }

        return $this->orderClient->order($purchasedProductList);
    }
}