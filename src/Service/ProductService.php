<?php

namespace App\Service;

use App\Client\CatalogueClient;
use App\Client\StockClient;
use App\Entity\DTO\AvailableProductDTO;

class ProductService
{
    public function __construct(
        private CatalogueClient $catalogueClient,
        private StockClient     $stockClient,
    )
    {
    }

    public function getProduct(string $productId): array
    {
        return $this->catalogueClient->getProduct($productId);
    }

    public function getListProducts(): array
    {
        //Appel à l'api des stocks
        $responseStock = $this->stockClient->getProducts();

        //Appel à l'api Catalogue
        $responseCatalogue = $this->catalogueClient->getProducts();

        $availableStock = [];

        foreach ($responseStock as $productStock) {
            //On ne traite que les produits en stock
            if ($productStock['quantity'] !== 0) {
                $product = new AvailableProductDTO();
                $product->setId($productStock['productId']);

                $productCatalogue = $this->catalogueClient->getProduct($product->getId());

                $product->setName($productCatalogue['name']);
                $product->setDescription($productCatalogue['description']);
                $product->setUnitPrice($productCatalogue['price']);

                $availableStock[] = $product;
            }
        }
        return $availableStock;
    }
}