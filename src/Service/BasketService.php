<?php

namespace App\Service;

use App\Entity\Basket;
use App\Entity\DTO\BasketDTO;
use App\Entity\DTO\ProductAddedToBasketDTO;
use App\Repository\BasketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class BasketService
{
    public function __construct(
        private BasketRepository       $basketRepository,
        private EntityManagerInterface $manager,
        private ProductService         $productService,
        private NormalizerInterface    $normalizer,
    )
    {
    }

    public function findOrCreateBasket(): Basket
    {
        if (!$basket = $this->basketRepository->findOneBy([])) {
            $basket = new Basket();

            $this->manager->persist($basket);
            $this->manager->flush();
        }

        return $basket;
    }

    public function addProduct(Basket $basket, array $product): Basket
    {
        $basket->addProduct($product);

        $this->manager->persist($basket);
        $this->manager->flush();

        return $basket;
    }

    public function getBasketDetails(Basket $basket): array
    {
        $totalPrice = 0;
        $productList = [];

        foreach ($basket->getProducts() as $productId => $quantity) {
            $retrievedProduct = $this->productService->getProduct($productId);

            $totalPrice += (int)$retrievedProduct['price'];

            $productAddedToBasket = new ProductAddedToBasketDTO();

            $productAddedToBasket->id = $productId;
            $productAddedToBasket->name = $retrievedProduct['name'];
            $productAddedToBasket->description = $retrievedProduct['description'];
            $productAddedToBasket->unitPrice = $retrievedProduct['price'];
            $productAddedToBasket->quantity = $quantity;

            $productList[] = $this->normalizer->normalize($productAddedToBasket, 'json');
        }

        $basket = new BasketDTO();

        $basket->totalPrice = $totalPrice;
        $basket->products = $productList;

        return $this->normalizer->normalize($basket, 'json');
    }

    public function isBasketEmpty(Basket $basket): bool
    {
        return $basket->getProducts()->isEmpty();
    }


}