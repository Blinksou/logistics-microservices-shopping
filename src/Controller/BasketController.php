<?php

namespace App\Controller;

use App\Entity\DTO\Input\AddToBasketRequestDTO;
use App\Service\BasketService;
use App\Service\OrderService;
use App\Service\ProductService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasketController extends AbstractController
{
    public function __construct(
        private BasketService  $basketService,
        private ProductService $productService,
        private OrderService   $orderService
    )
    {
    }

    #[Route("/api/basket", name: "basket.add", methods: Request::METHOD_PUT)]
    public function addToBasket(AddToBasketRequestDTO $addToBasketRequestDTO): JsonResponse
    {
        $basket = $this->basketService->findOrCreateBasket();

        try {
            $this->productService->getProduct($addToBasketRequestDTO->id);
        } catch (Exception) {
            return $this->json([], Response::HTTP_BAD_REQUEST);
        }

        $this->basketService->addProduct(
            $basket,
            [
                'id' => $addToBasketRequestDTO->id,
                'quantity' => $addToBasketRequestDTO->quantity
            ]
        );
        
        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    #[Route("/api/basket", name: "basket.details", methods: Request::METHOD_GET)]
    public function getBasketDetails(): JsonResponse
    {
        $basket = $this->basketService->findOrCreateBasket();

        return $this->json(
            $this->basketService->getBasketDetails($basket)
        );
    }

    #[Route("/api/basket/checkout", name: "basket.checkout", methods: Request::METHOD_POST)]
    public function checkout(): JsonResponse
    {
        $basket = $this->basketService->findOrCreateBasket();

        if ($this->basketService->isBasketEmpty($basket)) {
            return $this->json('Impossible de checkout car votre panier est vide.', Response::HTTP_PRECONDITION_FAILED);
        }

        try {
            $order = $this->orderService->order($basket);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), Response::HTTP_SERVICE_UNAVAILABLE);
        }

        return $this->json(
            $order['id'],
            Response::HTTP_OK
        );
    }


}