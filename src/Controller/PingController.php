<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/api/ping", name: "api.ping", methods: Request::METHOD_GET)]
class PingController extends AbstractController
{
    public function __invoke(): JsonResponse
    {
        return $this->json(['message' => 'Pong !']);
    }
}