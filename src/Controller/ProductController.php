<?php

namespace App\Controller;

use App\Service\ProductService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    public function __construct(
        private ProductService $productService,
    )
    {
    }

    #[Route("/api/products", name: "products.listProducts", methods: Request::METHOD_GET)]
    public function listProducts(): JsonResponse
    {
        try {
            $products = $this->productService->getListProducts();
        } catch (Exception $e) {
            return $this->json($e->getMessage(), Response::HTTP_SERVICE_UNAVAILABLE);
        }

        return $this->json(
            $products
        );
    }
}