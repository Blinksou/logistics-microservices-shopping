<?php

namespace App\Entity;

use App\Repository\BasketRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BasketRepository::class)
 */
class Basket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="json")
     */
    private array $products;

    public function __construct()
    {
        $this->products = [];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function addProduct(array $product): self
    {
        if (!isset($this->products[$product['id']])) {
            $this->products[$product['id']] = $product['quantity'];
        } else {
            $this->products[$product['id']] += $product['quantity'];
        }

        return $this;
    }

    public function removeProduct(array $product): self
    {
        return $this;
    }
}
