<?php

namespace App\Entity\DTO\Output;

use Nelexa\RequestDtoBundle\Dto\RequestObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

// cf https://github.com/Ne-Lexa/RequestDtoBundle
class PurchasedProductDto implements RequestObjectInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public string $id;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public int $quantity;
}