<?php

namespace App\Entity\DTO;

class BasketDTO
{
    public int $totalPrice = 0;

    public array $products = [];
}