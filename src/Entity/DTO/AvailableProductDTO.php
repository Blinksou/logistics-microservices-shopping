<?php

namespace App\Entity\DTO;

use PhpParser\Node\Expr\Cast\Double;

class AvailableProductDTO
{
    public int $id;

    public string $name;

    public string $description;

    public Double $unitPrice;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }
}
