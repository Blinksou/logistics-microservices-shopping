<?php

namespace App\Entity\DTO\Input;

use Nelexa\RequestDtoBundle\Dto\RequestObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

// cf https://github.com/Ne-Lexa/RequestDtoBundle
class AvailableProductRequestDTO implements RequestObjectInterface
{
    /** @Assert\NotBlank() */
    public string $id = '';

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public string $name = '';

    public string $description = '';

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?int $unitPrice = null;
}