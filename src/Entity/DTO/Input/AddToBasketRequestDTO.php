<?php

namespace App\Entity\DTO\Input;

use Nelexa\RequestDtoBundle\Dto\RequestBodyObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

// cf https://github.com/Ne-Lexa/RequestDtoBundle
class AddToBasketRequestDTO implements RequestBodyObjectInterface
{
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $id = '';

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public int $quantity = 1;
}