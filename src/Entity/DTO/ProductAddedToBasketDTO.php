<?php

namespace App\Entity\DTO;

class ProductAddedToBasketDTO
{
    public string $id = '';

    public string $name = '';

    public string $description = '';

    public ?int $unitPrice = null;

    public ?int $quantity = null;
}