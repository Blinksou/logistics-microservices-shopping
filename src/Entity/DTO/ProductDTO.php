<?php

namespace App\Entity\DTO;

use Nelexa\RequestDtoBundle\Dto\RequestObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

// cf https://github.com/Ne-Lexa/RequestDtoBundle
class ProductDTO implements RequestObjectInterface
{
    /** @Assert\NotBlank() */
    public string $id = '';

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public string $ean;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public string $name = '';

    public string $description = '';

    /**
     * @Assert\NotNull()
     */
    public int $price;

    /**
     * @Assert\Optional(@Assert\NotBlank())
     */
    public ?array $categories = null;
}