<?php

namespace App\Tests\Acceptance;

use App\Tests\WebTestCaseWithDatabase;
use EnricoStahn\JsonAssert\AssertClass as JsonAssert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BasketTest extends WebTestCaseWithDatabase
{
    public const PRODUCT_ID = "61899e05c973f0bd0a6c4dda";

    public function testAddToBasketExistingProduct(): void
    {
        /* $productId = null;

         $this->client->request(Request::METHOD_GET, '/api/products/' . $productId);*/

//        $product = $this->client->getResponse()->getContent();

        $this->client->request(Request::METHOD_PUT, '/api/basket', [
                'id' => self::PRODUCT_ID,
                'quantity' => 1
            ]
        );

        self::assertResponseStatusCodeSame(Response::HTTP_NO_CONTENT);
    }

    public function testAddToBasketUnknownProduct(): void
    {
        $productId = "blabla";

        $this->client->request(Request::METHOD_PUT, '/api/basket', [
                'id' => $productId,
                'quantity' => 1
            ]
        );

        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    public function testGetBasketDetails(): void
    {
        $this->client->request(Request::METHOD_GET, '/api/basket');

        $response = $this->client->getResponse()->getContent();

        $content = json_decode($response, true);

        self::assertResponseIsSuccessful();

        JsonAssert::assertJsonMatchesSchema($content, __DIR__ . './Schema/BasketDetails.json');
    }

    public function testNotEmptyBasketCheckout(): void
    {
        $this->client->request(Request::METHOD_POST, '/api/basket/checkout');
        
        self::assertResponseIsSuccessful();
    }
}