<?php

namespace App\Tests\Acceptance;

use App\Tests\WebTestCaseWithDatabase;

class PingTest extends WebTestCaseWithDatabase
{
    public function testPing(): void
    {
        $this->client->request('GET', '/api/ping');

        self::assertResponseIsSuccessful();
        
        self::containsEqual(['message' => 'Pong !']);
    }
}